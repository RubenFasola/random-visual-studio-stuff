# Random Visual Studio Stuff


## Description
the name is self-describing


## Installation
1. clone repo
2. open Visual studio (this only needs the desktop C++ dev workflow)
3. compile (ctrl+b is your friend)
4. ???
5. profit.

## Usage
extract BSA and ESP files, and more in the future

## License
the Unlicense

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

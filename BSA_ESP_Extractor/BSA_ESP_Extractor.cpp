#include <iostream>
#include <fstream>
#include <chrono>

#define byte char
#define BSA "BSA"
#define TES4 "TES4"


using namespace std;
using namespace std::chrono;

string char_arr_to_str(char* char_arr);

unsigned long index_of(byte* mem_block, unsigned long block_length, const byte* search_term, unsigned long search_term_length);

unsigned long get_file_size(string file_path);

int main(int argc, char* argv[])
{
	time_point<high_resolution_clock> global_start, global_end, i_start, i_end, o_start, o_end;
	global_start = high_resolution_clock::now();
	string file_path;
	if (argc >= 2)
	{
		file_path = char_arr_to_str(argv[1]);
		cout << file_path << endl;
	}
	else
	{
		return -2;
	}

	byte* in_memory_file = 0x0;

	i_start = high_resolution_clock::now();
	unsigned long size = get_file_size(file_path);
	in_memory_file = (byte*)malloc(size);
	fstream file;
	file.open(file_path, ios::in | ios::binary);
	file.read(in_memory_file, size);//probably the longest op in the software, I hate IO
	file.close();
	i_end = high_resolution_clock::now();

	bool contains_BSA;
	unsigned long BSA_index;

	try
	{
		BSA_index = index_of(in_memory_file, 100, BSA, 3);
		contains_BSA = true;
	}
	catch (const exception&)
	{
		contains_BSA = false;
	}

	unsigned long TES4_index;

	if (contains_BSA)
	{
		try
		{
			TES4_index = index_of(in_memory_file, size, TES4, 4);
		}
		catch (const exception&)
		{
			return -1;
		}
	}
	else
	{
		try
		{
			TES4_index = index_of(in_memory_file, 100, TES4, 4);
		}
		catch (const exception&)
		{
			return -1;
		}
	}

	cout << "bsa index: " << (contains_BSA ? BSA_index : 0) << endl << "tes4 index: " << TES4_index << endl;
	
	o_start = high_resolution_clock::now();
	string output_file_path;

	if (contains_BSA)
	{
		output_file_path = file_path + ".bsa"; //lazy append, resulting file will be .bin.bsa
		file.open(output_file_path, ios::out | ios::binary);
		file.write(in_memory_file + BSA_index, TES4_index - BSA_index);
		file.close();
	}

	output_file_path = file_path + ".esp";
	file.open(output_file_path, ios::out | ios::binary);
	file.write(in_memory_file + TES4_index, size - TES4_index);
	file.close();
	o_end = high_resolution_clock::now();

	global_end = high_resolution_clock::now();
	cout << "total execution time: " << duration_cast<microseconds>(global_end - global_start).count() << endl << "io time: " << duration_cast<microseconds>(i_end - i_start).count() + duration_cast<microseconds>(o_end - o_start).count() << endl;
	
	system("pause");
	return 0;
}

string char_arr_to_str(char* char_arr)
{
	string str;
	long i = 0;
	do
	{
		str += *(char_arr + i);//char_arr[i]
		i++;
	} while (*(char_arr + i) != NULL);
	return str;
}

/// <summary>
/// get the index of an arbitrary lenght collection of bytes in an arbitrary lenght collection of bytes
/// </summary>
/// <param name="mem_block"></param>
/// <param name="block_length"></param>
/// <param name="search_term"></param>
/// <param name="search_term_length"></param>
/// <returns></returns>
unsigned long index_of(byte* mem_block, unsigned long block_length, const byte* search_term, unsigned long search_term_length)
{
	for (unsigned long iul = 0; iul < block_length; iul++)
	{
		if (*(mem_block + iul) == *search_term)
		{
			for (unsigned long search_index = 0; search_index < search_term_length;)
			{
				if (*(mem_block + iul + search_index) != *(search_term + search_index))
				{
					break;
				}
				search_index++;
				if (search_index == search_term_length)
				{
					return iul;
				}
			}
		}
	}
	throw out_of_range("value not found");
}

unsigned long get_file_size(string file_path)
{
	unsigned long begin, end;
	fstream file;
	file.open(file_path, ios::in | ios::binary);
	file.seekg(0, ios::beg);
	begin = file.tellg();
	file.seekg(0, ios::end);
	end = file.tellg();
	file.close();
	return end - begin;
}
